(function($) {
	$(document).ready(function() {
        
        
		//**************************************************
		//***** Email Signature Master Control Program *****
		//**************************************************
		$("#inputFullname").keyup(function() {
			$("#outputFullname").html($(this).val());
		});
        
		$("#inputTitle").keyup(function() {
			$("#outputTitle").html($(this).val());
		});
        
		$("#inputEmail").keyup(function() {
			if ($(this).val() != '') {
				$("#outputEmail").html($(this).val());
				$("#outputEmailLink").attr("href", "mailto:" + $(this).val());
			} else {
				$("#outputEmail").html('');
			}
		});
        
		$("#inputOffice").keyup(function() {
			if ($(this).val() != '') {
				$("#outputOffice").html($(this).val());
				$("#outputOffice").prepend("<span style='color: #DA291C; font-weight:bold;'>o</span>&nbsp;&nbsp;&nbsp;");
			} else {
				$("#outputOffice").html('');
			}
		});
        
		$("#inputMobile").keyup(function() {
			if ($(this).val() != '') {
				$("#outputMobile").html($(this).val());
				$("#outputMobile").prepend("<span style='color: #DA291C; font-weight: bold;'>m</span>&nbsp;&nbsp;");
			} else {
				$("#outputMobile").html('');
			}
		});
        
		$("#inputTwitter").keyup(function() {
			if ($(this).val() != '') {
				$("#outputTwitter").html($(this).val());
				$("#outputTwitter").prepend("<span><img src='http://rs.ivanti.com/images/email-signature/twitter_icon.png' /></span>&nbsp;&nbsp;");
				//$("#paragraph2parent").append("<p class=MsoNormal id='paragraph2' style='background:white; height: 6px; margin: 0; padding: 0;'><span style='font-size:6.0pt;'>&nbsp;</span></p>");
			} else {
				$("#outputTwitter").html('');
				//$("#paragraph2").remove();
			}
		});
		//        $("#inputMobile").keyup(function() {
		//            if ($(this).val() != '') {
		//							if ($("#inputOffice").val() == 'xxx.xxx.xxxx') {
		//								$("#outputMobile").html("m: " + $(this).val());
		//							}
		//							else {
		//            		$("#outputMobile").html(" | m: " + $(this).val());
		//							}
		//            }
		//					 	else {
		//              	$("#outputMobile").html('');
		//            }
		//        });
        
		$("#inputBannerCaption").keyup(function() {
			$("#outputBannerCaption").html($(this).val());
		});
        
		$("#inputCompany").change(function() {
			$("#outputWebsite").html("");
			$("#outputWebsiteLink").attr("href", "");
			$("#outputWebsite2").html("");
			$("#outputWebsiteLink2").attr("href", "");
			$("#outputWebsite3").html("");
			$("#outputWebsiteLink3").attr("href", "");
			$("#outputWebsite4").html("");
			$("#outputWebsiteLink4").attr("href", "");
			$(".appSenseTagLine").html("");
			if ($(this).val() === 'allCompanies') // ALL COMPANIES
			{
				$("#inputEmail").val('first.last@landesk.com');
				$("#outputEmail").html('first.last@landesk.com');
				$("#outputWebsite").html("www.landesk.com");
				$("#outputWebsiteLink").attr("href", "http://www.landesk.com");
				$("#outputWebsite2").html(" | www.shavlik.com");
				$("#outputWebsiteLink2").attr("href", "http://www.shavlik.com");
				$("#outputWebsite3").html(" | www.wavelink.com");
				$("#outputWebsiteLink3").attr("href", "http://www.wavelink.com");
				$("#outputWebsite4").html(" | www.appsense.com");
				$("#outputWebsiteLink4").attr("href", "http://www.appsense.com");
				$("#outputLogo").attr("src", "http://rs.landesk.com/images/email-signature/all_business_logos.jpg");
			} else if ($(this).val() === 'appsense' || $(this).val() === 'appsenseUK') // APPSENSE
			{
				$("#inputEmail").val('');
				$("#inputEmail").val('first.last@appsense.com');
				$("#outputEmail").html('first.last@appsense.com');
				// var appsenseEmail = $( "#inputEmail").val();
				// $( "#outputFullnameASLink" ).attr("href", "mailto: " + appsenseEmail);
				$("#outputWebsite").html("www.appsense.com");
				$("#outputWebsiteLink").attr("href", "http://www.appsense.com");
				$("#outputLogo").attr("src", "http://rs.landesk.com/images/email-signature/appsense-email-logo.png");
				if ($(this).val() === 'appsense') {
					$(".appSenseTagLine").html("");
				} else if ($(this).val() === 'appsenseUK') {
					$(".appSenseTagLine").html("AppSense Limited (England & Wales, 03873980)");
				}
			} else if ($(this).val() === 'wavelink') // WAVELINK
			{
				$(".appsenseSignature").hide();
				$(".generalSignature").show();
				$("#inputEmail").val('');
				$("#inputEmail").val('first.last@wavelink.com');
				$("#outputEmail").html('first.last@wavelink.com');
				$("#outputWebsite").html("www." + $(this).val() + ".com");
				$("#outputWebsiteLink").attr("href", "http://www." + $(this).val() + ".com");
				$("#outputLogo").attr("src", "http://rs.wavelink.com/images/" + $(this).val() + "-email-logo.jpg");
			} else if ($(this).val() === 'shavlik') // SHAVLIK
			{
				$(".appsenseSignature").hide();
				$(".generalSignature").show();
				$("#inputEmail").val('');
				$("#inputEmail").val('first.last@shavlik.com');
				$("#outputEmail").html('first.last@shavlik.com');
				$("#outputWebsite").html("www." + $(this).val() + ".com");
				$("#outputWebsiteLink").attr("href", "http://www." + $(this).val() + ".com");
				$("#outputLogo").attr("src", "http://rs.shavlik.com/images/" + $(this).val() + "-email-logo.jpg");
			} else { // LANDESK
				$(".appsenseSignature").hide();
				$(".generalSignature").show();
				$("#inputEmail").val('');
				$("#inputEmail").val('first.last@landesk.com');
				$("#outputEmail").html('first.last@landesk.com');
				$("#outputWebsite").html("www." + $(this).val() + ".com");
				$("#outputWebsiteLink").attr("href", "http://www." + $(this).val() + ".com");
				$("#outputLogo").attr("src", "http://rs.landesk.com/images/email-signature/" + $(this).val() + ".jpg");
			}
		});
        
		$("#inputBannerLink").keyup(function() {
			var link = $(this).val();
			if ($(this).val().search("http://") === -1) link = "http://" + link;
			$("#outputBannerLink").attr("href", link);
		});
        
		// Change banner image and link on drop down selection change
		$("#inputBanner").change(function() {
			var link = "";
			//            if ($(this).val().search("Select") != -1) {
			//                $("#outputBanner").css("display", "none");
			//            } else {
			//                $("#outputBanner").css("display", "inline");
			//                $("#outputBanner").attr("src", "http://rs.landesk.com/images/email-signature/" + $(this).val() + ".jpg");
			//                if ($(this).val().search("shavlik") != -1) {
			//                    if ($(this).val().search("bestofshow") != -1)
			//                        link = "http://blog.shavlik.com/shavlik-protect-vmworld-2016-security-gold-award-winner/";
			//                    else if ($(this).val().search("patchtuesday") != -1)
			//                        link = "http://www.shavlik.com/patch-tuesday/";
			//                } 
			//								else if ($(this).val().search("appsense-endpoint") != -1)
			//                 	link = "http://www.appsense.com/products/endpoint-security-suite/";
			//                else if ($(this).val().search("campaign") != -1) {
			//                    if ($(this).val().search("ransomware") != -1)
			//                        link = "http://blog.landesk.com/en/infographic-the-8-scariest-stats-about-ransomware/";
			//                    else if ($(this).val().search("itam") != -1)
			//                        link = "http://blog.landesk.com/en/former-gartner-analyst-talks-about-landesks-itam-champion-award/";
			//                } 
			//								else if ($(this).val().search("secure-email") != -1)
			//                    link = "http://info.landesk.com/secureemail.html";
			//                else if ($(this).val().search("community") != -1)
			//                    link = "https://community.landesk.com/docs/DOC-34233";
			//                else if ($(this).val().search("webinar") != -1)
			//                    link = "http://www.landesk.com/webinars/";
			//								else if ($(this).val().search("landesk-heat") != -1) {
			//										$("#outputBanner").attr("src", "http://rs.landesk.com/images/email-signature/" + $(this).val() + ".png");
			//										link = "http://www.landesk.com/company/press-releases/2017/landesk-heat-software-clearlake-capital/";
			//								}
			//								else if ($(this).val().search("interchange2017") != -1) {
			//										$("#outputBanner").attr("src", "http://rs.landesk.com/images/email-signature/" + $(this).val() + ".png");
			//										link = "http://interchange.landesk.com/";
			//								}
			//            }
			if ($(this).val().search("Select") != -1) {
				$("#outputBanner").css("display", "none");
                
			} else {
				$("#outputBanner").css("display", "inline");
                
				$("#outputBanner").attr("src", "http://rs.ivanti.com/images/email-signature/" + $(this).val() + ".png");
                if ($(this).val().search("VMWorld") != -1) link = "http://go.ivanti.com/VMWorld2017.html";
                else if ($(this).val().search("Ivanti-Acquires") != -1) link = "https://www.ivanti.com/blog/video-ivanti-acquires-res-software/";
                else if ($(this).val().search("Ivanti-PeerInsights") != -1) link = "https://www.gartner.com/reviews/vendor/write/ivanti/?refval=vlp-quarterly-campaign";
				else if ($(this).val().search("WannaCrypt") != -1) link = "https://go.ivanti.com/Ransomware-Get-Well-Quick.html";
                else if ($(this).val().search("Interchange-2017") != -1) link = "http://interchange.ivanti.com/";
				else if ($(this).val().search("AS-is-now") != -1) link = "http://blog.appsense.com/2017/01/we-are-ivanti/";
                else if ($(this).val().search("CO-is-now") != -1) link = "https://www.ivanti.com/company/press-releases/2017/ivanti-acquires-concorde";
				else if ($(this).val().search("HS-is-now") != -1) link = "https://www.ivanti.com/blog/ivanti-faq/";
				else if ($(this).val().search("LD-is-now") != -1) link = "https://www.ivanti.com/blog/ivanti-faq/";
                else if ($(this).val().search("RES-is-now") != -1) link = "https://www.ivanti.com/blog/video-ivanti-acquires-res-software/";
				else if ($(this).val().search("SH-is-now") != -1) link = "https://www.ivanti.com/blog/ivanti-faq/";
				else if ($(this).val().search("WL-is-now") != -1) link = "http://blog.wavelink.com/2017/01/23/we-are-ivanti/";
				else if ($(this).val().search("We-are-now") != -1) link = "https://www.ivanti.com/blog/ivanti-faq/";
                else if ($(this).val().search("Momentum-Events") != -1) link = "https://community.ivanti.com/community/momentum/momentum-events";
                else if ($(this).val().search("Momentum-Share") != -1) link = "https://community.ivanti.com/community/momentum/share";
			}
			$("#outputBannerLink").attr("href", link);
			$("#inputBannerLink").val(link);
		});
        
		// Handles submit button click on correct password enter
		$(".frmSubmit").click(function() {
			var magicHappens = $("#sigPassword").val();
			if (magicHappens == "advertise1") {
				$('.passwordContent').hide("fast");
				$('#error').hide();
				$('.signatureContent').show("fast");
			} else {
				$('#error').hide();
				$('#error').show("fast");
				$('#error').html('<span style="color:#cc0000">Invalid password.</span>');
			}
		});
        
		// Handles submit button enable on correct password enter
		$(function() {
			$('#sigPassword').keypress(function(event) {
				if (event.which == 13) {
					var magicHappens = $("#sigPassword").val();
					if (magicHappens == "advertise1") {
						$('.passwordContent').hide("fast");
						$('#error').hide();
						$('.signatureContent').show("fast");
					} else {
						$('#error').hide();
						$('#error').show("fast");
						$('#error').html('<span style="color:#cc0000">Invalid password.</span>');
					}
					event.preventDefault();
				}
			});
		});
        
		// Function for on screen selection of email signature
		function SelectText(element) {
			var doc = document,
				text = doc.getElementById(element),
				range, selection;
			if (doc.body.createTextRange) {
				range = document.body.createTextRange();
				range.moveToElementText(text);
				range.select();
			} else if (window.getSelection) {
				selection = window.getSelection();
				range = document.createRange();
				range.selectNodeContents(text);
				selection.removeAllRanges();
				selection.addRange(range);
			}
		}
        
		// Handles on screen selection of email signature
		$(function() {
			$('#selectSignature').click(function() {
				SelectText('copyme');
			});
		});
        
        
		//*******************************************************
		//*****  Business Card Form Master Control Program  *****
		//*******************************************************
		// No business card toggle event handler
		$('#inputNoCard').change(function() {
			if ($(this).is(":checked")) {
				$('.businessCardForm').hide();
				$('.preview').hide();
				$('.noBusinessCardForm').show();
			} else {
				$('.businessCardForm').show();
				$('.preview').show();
				$('.noBusinessCardForm').hide();
			}
		});
        
		// Sort Address Dropdown list alphabetically
		$("#inputCardAddress1").html($("#inputCardAddress1 option").sort(function(a, b) {
			return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
		}));
		
        // Move "Choose...", "No Address", and "Custom" options up to the top of the select list
		$('#inputCardAddress1 option[value=""]').insertBefore('#inputCardAddress1 option[value="Entrada 501 1096 EH Amsterdam, Netherlands"]');
		$('#inputCardAddress1 option[value="None"]').insertBefore('#inputCardAddress1 option[value="Entrada 501 1096 EH Amsterdam, Netherlands"]');
		$('#inputCardAddress1 option[value="Custom"]').insertBefore('#inputCardAddress1 option[value="Entrada 501 1096 EH Amsterdam, Netherlands"]');
		
        // Make sure default value is set to "Choose..." option
		$("#inputCardAddress1").val('Choose...');
        
		// Allow preview to scroll with content as user scrolls
		$(window).scroll(function() {
			if ($(this).scrollTop() > 300) {
				$(".preview").addClass("fix-preview");
			} else {
				$(".preview").removeClass("fix-preview");
			}
		});
        
		$("#inputCardDoubleSided").change(function() {
			if (this.checked) {
				$("#languageInputs").show();
			} else {
				$("#languageInputs").hide();
			}
		});
        
		$("#inputCardFullname").keyup(function() {
			$("#outputCardFullname").html($(this).val());
		});
        
		// Adds comma between name and certification in business card preview
		$("#inputCardCertification").keyup(function() {
			$("#outputCardCertification").html($(this).val());
		});
        
		$("#inputCardTitle").keyup(function() {
			$("#outputCardTitle").html($(this).val());
		});
        
		$("#inputCardTitle2").keyup(function() {
			$("#outputCardTitle2").html($(this).val());
		});
        
		$("#inputCardTwitter").keyup(function() {
			$("#outputCardTwitter").html($(this).val());
		});
        
		// Set default phone number format
		jQuery(function($) {
			$("#inputCardPhone1").mask("(999) 999-9999");
			$("#inputCardPhone2").mask("(999) 999-9999");
			$("#inputCardPhone3").mask("(999) 999-9999");
		});
        
		// Address select change event handler
		$("#inputCardAddress1").change(function() {
			if ($(this).val() != '') {
				$(".customAddress").hide();
				var address = $(this).val();
				if (address == "None") {
					address = "";
					$("#inputCardPhone1").unmask();
					$("#inputCardPhone2").unmask();
					$("#inputCardPhone3").unmask();
					$("#outputCardAddress1").css("display", "none");
				} else if (address == "Custom") {
					address = "";
					$("#inputCardPhone1").unmask();
					$("#inputCardPhone2").unmask();
					$("#inputCardPhone3").unmask();
					$(".customAddress").show();
				} else if (address == "698 West 10000 South Suite 500, South Jordan, Utah 84095") {
					address = "698 West 10000 South Suite 500<br />South Jordan, Utah 84095";
					$("#inputCardPhone1").mask("(999) 999-9999");
					$("#inputCardPhone2").mask("(999) 999-9999");
					$("#inputCardPhone3").mask("(999) 999-9999");
				} else if (address == "1011 Western Avenue, Suite 700, Seattle, Washington 98104") {
					address = "1011 Western Avenue, Suite 700<br />Seattle, Washington 98104";
					$("#inputCardPhone1").mask("(999) 999-9999");
					$("#inputCardPhone2").mask("(999) 999-9999");
					$("#inputCardPhone3").mask("(999) 999-9999");
				} else if (address == "119 14th Street NW, Suite 200, New Brighton, Minnesota 55112") {
					address = "119 14th Street NW, Suite 200<br />New Brighton, Minnesota 55112";
					$("#inputCardPhone1").mask("(999) 999-9999");
					$("#inputCardPhone2").mask("(999) 999-9999");
					$("#inputCardPhone3").mask("(999) 999-9999");
				} else if (address == "1712 Main Street Office 3 Kansas City, MO 64108") {
					address = "1712 Main Street Office 3 Kansas City, MO 64108";
					$("#inputCardPhone1").mask("(999) 999-9999");
					$("#inputCardPhone2").mask("(999) 999-9999");
					$("#inputCardPhone3").mask("(999) 999-9999");
				} else if (address == "3 Arlington Square Downshire Way, Bracknell Berkshire RG12 1WA") {
					address = "3 Arlington Square Downshire Way<br />Bracknell Berkshire RG12 1WA";
					$("#inputCardPhone1").mask("+99 (0)9999 999999");
					$("#inputCardPhone2").mask("+99 (0)9999 999999");
					$("#inputCardPhone3").mask("+99 (0)9999 999999");
				} else if (address == "3200 Daresbury Park, Daresbury Warrington WA4 4BU, United Kingdom") {
					address = "3200 Daresbury Park, Daresbury Warrington<br />WA4 4BU, United Kingdom";
					$("#inputCardPhone1").mask("+99 999 999 9999");
					$("#inputCardPhone2").mask("+99 999 999 9999");
					$("#inputCardPhone3").mask("+99 999 999 9999");
				} else if (address == "Insurgentes Sur 800 piso 8, Col. del Valle, Mexico D.F. - Mexico 03100") {
					address = "Insurgentes Sur 800 piso 8, Col. del Valle<br />Mexico D.F. - Mexico 03100";
					$("#inputCardPhone1").mask("+99 99 9999 9999");
					$("#inputCardPhone2").mask("+99 99 9999 9999");
					$("#inputCardPhone3").mask("+99 99 9999 9999");
				} else if (address == "Regus Paseo Interlomas Vialidad de la Barranca No 6 Piso 4 Col. Ex Hacienda de Jesus del Monte Mexico. C.P. 52772") {
					address = "Regus Paseo Interlomas Vialidad de la Barranca No 6 Piso 4<br />Col. Ex Hacienda de Jesus del Monte Mexico. C.P. 52772";
					$("#inputCardPhone1").mask("+99 99 9999 9999");
					$("#inputCardPhone2").mask("+99 99 9999 9999");
					$("#inputCardPhone3").mask("+99 99 9999 9999");
				} else if (address == "Av Paulista, 2300, São Paulo, SP, 01310-300, Brasil") {
					address = "Av Paulista, 2300<br />São Paulo, SP, 01310-300, Brasil";
					$("#inputCardPhone1").mask("+99 99 9 9999 9999");
					$("#inputCardPhone2").mask("+99 99 9 9999 9999");
					$("#inputCardPhone3").mask("+99 99 9 9999 9999");
				} else if (address == "LYONER STR. 12 . GER-60528 FRANKFURT AM MAIN") {
					address = "LYONER STR. 12 . GER-60528 FRANKFURT AM MAIN";
					$("#inputCardPhone1").unmask();
					$("#inputCardPhone2").unmask();
					$("#inputCardPhone3").unmask();
					$("#inputCardPhone1").val("+");
					$("#inputCardPhone2").val("+");
					$("#inputCardPhone3").val("+");
				} else if (address == "Werner-von-Siemens-Ring 17 85630 Grasbrunn/Munchen Deutschland") {
					address = "Werner-von-Siemens-Ring 17<br />85630 Grasbrunn/München Deutschland";
					$("#inputCardPhone1").unmask();
					$("#inputCardPhone2").unmask();
					$("#inputCardPhone3").unmask();
					$("#inputCardPhone1").val("+");
					$("#inputCardPhone2").val("+");
					$("#inputCardPhone3").val("+");
				} else if (address == "Fischhof 3/6 1010 Wien") {
					address = "Fischhof 3/6 1010 Wien";
					$("#inputCardPhone1").unmask();
					$("#inputCardPhone2").unmask();
					$("#inputCardPhone3").unmask();
				} else if (address == "Piazza 4 Novembre, 7 | 20124 Milano | Italia") {
					address = "Piazza 4 Novembre, 7 | 20124 Milano | Italia";
					$("#inputCardPhone1").mask("+99 999 9999999");
					$("#inputCardPhone2").mask("+99 999 9999999");
					$("#inputCardPhone3").mask("+99 999 9999999");
				} else if (address == "Immeuble Le Monge, 22 Place des Vosges, 92979 La Défense Cedex | France") {
					address = "Immeuble Le Monge, 22 Place des Vosges,<br />92979 La Défense Cedex | France";
					$("#inputCardPhone1").mask("+99 (0)9 99 99 99 99");
					$("#inputCardPhone2").mask("+99 (0)9 99 99 99 99");
					$("#inputCardPhone3").mask("+99 (0)9 99 99 99 99");
				} else if (address == "First Floor, Europa House, Harcourt Street, Dublin 2 Ireland") {
					address = "First Floor, Europa House,<br />Harcourt Street, Dublin 2 Ireland";
					$("#inputCardPhone1").mask("+999 (0)9 999 9999");
					$("#inputCardPhone2").mask("+999 (0)9 999 9999");
					$("#inputCardPhone3").mask("+999 (0)9 999 9999");
				} else if (address == "Rybna 682/14, Prague 1 Czech Republic") {
					address = "Rybna 682/14, Prague 1 Czech Republic";
					$("#inputCardPhone1").mask("+999 999 999 999");
					$("#inputCardPhone2").mask("+999 999 999 999");
					$("#inputCardPhone3").mask("+999 999 999 999");
				} else if (address == "Spain") {
					address = "";
					$("#inputCardPhone1").mask("+99 (0)99 999 9999");
					$("#inputCardPhone2").mask("+99 (0)99 999 9999");
					$("#inputCardPhone3").mask("+99 (0)99 999 9999");
				} else if (address == "Koujimachi Ichihara Bldg.5F, 1-1-8 Hirakawa-cho, Chiyoda-ku, Tokyo 102-0093 Japan") {
					address = "Koujimachi Ichihara Bldg.5F, 1-1-8 Hirakawa-cho,<br />Chiyoda-ku, Tokyo 102-0093 Japan";
					$("#inputCardPhone1").mask("(99)9-99999999");
					$("#inputCardPhone2").mask("(99)9-99999999");
					$("#inputCardPhone3").mask("(99)9-99999999");
				} else if (address == "Suite 2, Level 9, 2 Elizabeth Plaza, North Sydney, NSW, 2060") {
					address = "Suite 2, Level 9, 2 Elizabeth Plaza,<br />North Sydney, NSW, 2060";
					$("#inputCardPhone1").mask("+99 999 999 999");
					$("#inputCardPhone2").mask("+99 999 999 999");
					$("#inputCardPhone3").mask("+99 999 999 999");
				} else if (address == "St Kilda Towers, Suite 244, 1 Queens Road, Melbourne VIC 3004 Australia") {
					address = "St Kilda Towers, Suite 244, 1 Queens Road,<br />Melbourne VIC 3004 Australia";
					$("#inputCardPhone1").unmask();
					$("#inputCardPhone2").unmask();
					$("#inputCardPhone3").unmask();
				} else if (address == "Rua Tomas Da Fonseca, Torre G-Piso,1, 1600-209 Lisboa | PORTUGAL") {
					address = "Rua Tomas Da Fonseca, Torre G-Piso,1,<br />1600-209 Lisboa | PORTUGAL";
					$("#inputCardPhone1").mask("+999 999 999 999");
					$("#inputCardPhone2").mask("+999 999 999 999");
					$("#inputCardPhone3").mask("+999 999 999 999");
				} else if (address == "7 Menachem Begin Road, Ramat Gan, Israel") {
					address = "7 Menachem Begin Road, Ramat Gan, Israel";
					$("#inputCardPhone1").unmask();
					$("#inputCardPhone2").unmask();
					$("#inputCardPhone3").unmask();
				} else if (address == "ul. Polna 11, 00-633 Warsaw, building: IBC II, 1st floor") {
					address = "ul. Polna 11, 00-633 Warsaw, building: IBC II, 1st floor";
					$("#inputCardPhone1").mask("+99 999 999 999");
					$("#inputCardPhone2").mask("+99 999 999 999");
					$("#inputCardPhone3").mask("+99 999 999 999");
				} else if (address == "Entrada 501 1096 EH Amsterdam, Netherlands") {
					address = "Entrada 501 1096 EH Amsterdam, Netherlands";
					$("#inputCardPhone1").mask("+99 99 9999999");
					$("#inputCardPhone2").mask("+99 99 9999999");
					$("#inputCardPhone3").mask("+99 99 9999999");
				} else if (address == "Office no. 1-1591G, Kistagangen 16, 164 40 Kista, Sweden") {
					address = "Office no. 1-1591G, Kistagangen 16, 164 40 Kista, Sweden";
					$("#inputCardPhone1").unmask();
					$("#inputCardPhone2").unmask();
					$("#inputCardPhone3").unmask();
				} else if (address == "Wangz Business Centre Suite #11, 8, 20 7 temasek Blvd Suntec Tower One Singapore 038987") {
					address = "Wangz Business Centre Suite #11, 8, 20 7<br />temasek Blvd Suntec Tower One Singapore 038987";
					$("#inputCardPhone1").unmask();
					$("#inputCardPhone2").unmask();
					$("#inputCardPhone3").unmask();
				} else if (address == "Beijing, China") {
					address = "北京市东城区东长安街1号<br />东方广场W1座5楼511单元 邮编：100738";
					$(".addressValue").val("北京市东城区东长安街1号<br />东方广场W1座5楼511单元 邮编：100738");
					$("#inputCardPhone1").unmask();
					$("#inputCardPhone2").unmask();
					$("#inputCardPhone3").unmask();
				} else if (address == "Unit 19A, 19F Feng Wei Tower, No. 108 Si You Xin Ma Lu Street, Yue Xiu  District, Guangzhou 510600, China") {
					address = "广州市越秀区寺右新马路108号丰伟大厦19A房";
					$("#inputCardPhone1").unmask();
					$("#inputCardPhone2").unmask();
					$("#inputCardPhone3").unmask();
				} else if (address = "Shanghai Silver Tower Room  3027 3F No. 85 Taoo Yuan Road, Lu Wan District Shanghai 200021 China") {
					address = "Shanghai Silver Tower Room  3027 3F No. 85 Taoo Yuan Road,<br />Lu Wan District Shanghai 200021 China";
					$("#inputCardPhone1").unmask();
					$("#inputCardPhone2").unmask();
					$("#inputCardPhone3").unmask();
				}
				$("#outputCardAddress1").html(address);
				$("#outputCardAddress1").css("display", "inline-block");
			} else {
				$("#inputCardPhone1").unmask();
				$("#inputCardPhone2").unmask();
				$("#inputCardPhone3").unmask();
				$("#outputCardAddress1").css("display", "none");
			}
		});
        
		$("#inputCardEmail").keyup(function() {
			$("#outputCardEmail").html($(this).val());
		});
        
		$("#inputCardSocial").keyup(function() {
			$("#outputCardSocial").html($(this).val());
		});
        
		$("#inputCardPhone1").keyup(function() {
			if ($(this).val() != '') {
				var phoneType = $('#inputPhoneType1').val();
				if (phoneType == 'O') {
					$("#outputCardPhone1").html("O: " + $(this).val());
				} else if (phoneType == 'M') {
					$("#outputCardPhone1").html("M: " + $(this).val());
				} else if (phoneType == 'F') {
					$("#outputCardPhone1").html("F: " + $(this).val());
				}
			} else {
				$("#outputCardPhone1").html('');
			}
		});
        
		$("#inputCardPhone2").keyup(function() {
			if ($(this).val() != '') {
				var phoneType = $('#inputPhoneType2').val();
				if (phoneType == 'O') {
					$("#outputCardPhone2").html("O: " + $(this).val());
				} else if (phoneType == 'M') {
					$("#outputCardPhone2").html("M: " + $(this).val());
				} else if (phoneType == 'F') {
					$("#outputCardPhone2").html("F: " + $(this).val());
				}
			} else {
				$("#outputCardPhone2").html('');
			}
		});
        
		$("#inputCardPhone3").keyup(function() {
			if ($(this).val() != '') {
				var phoneType = $('#inputPhoneType3').val();
				if (phoneType == 'O') {
					$("#outputCardPhone3").html("O: " + $(this).val());
				} else if (phoneType == 'M') {
					$("#outputCardPhone3").html("M: " + $(this).val());
				} else if (phoneType == 'F') {
					$("#outputCardPhone3").html("F: " + $(this).val());
				}
			} else {
				$("#outputCardPhone3").html('');
			}
		});
        
		$("#inputPhoneType1").change(function() {
			var phoneNumber = $("#inputCardPhone1").val();
			var phoneType = $(this).val();
			$("#outputCardPhone1").html(phoneType + ": " + phoneNumber);
		});
        
		$("#inputPhoneType2").change(function() {
			var phoneNumber = $("#inputCardPhone2").val();
			var phoneType = $(this).val();
			$("#outputCardPhone2").html(phoneType + ": " + phoneNumber);
		});
        
		$("#inputPhoneType3").change(function() {
			var phoneNumber = $("#inputCardPhone3").val();
			var phoneType = $(this).val();
			$("#outputCardPhone3").html(phoneType + ": " + phoneNumber);
		});
        
		$("#inputCardFullnameLanguage").keyup(function() {
			$("#outputCardFullNameLanguage").html($(this).val());
		});
        
		$("#inputCardCertificationLanguage").keyup(function() {
			$("#outputCardCertificationLanguage").html($(this).val());
		});
        
		$("#inputCardTitleLanguage").keyup(function() {
			$("#outputCardTitleLanguage").html($(this).val());
		});
        
		// Display back side of business card
		$(function() {
			$("#cardType").change(function() {
				$(".cardBackSide").toggle(this.checked);
			}).change(); //ensure visible state matches initially
		});
        
		// Handler for form input placeholders
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
				input.removeClass('placeholder');
			}
		}).blur(function() {
			var input = $(this);
			if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
			}
		}).blur();
        
		// Get department value and store in hidden input field for Google spreadsheet
		$(".department").change(function() {
			var department = $(this).val();
			$('.departmentValue').val(department);
		});
        
		// Get address value and store in hidden input field for Google spreadsheet
		$(".address").change(function() {
			var address = $(this).val();
			$('.addressValue').val(address);
		});
		$(".customAddress").keyup(function() {
			$("#outputCardAddress1").html($('#inputCustomAddress').val());
		});
        
		// Get phone type vales and store them in hidden input fields for Google spreadsheet
		$(".phonetype1").change(function() {
			var phonetype1 = $(this).val();
			$('.phone1TypeValue').val(phonetype1);
		});
		$(".phonetype2").change(function() {
			var phonetype2 = $(this).val();
			$('.phone2TypeValue').val(phonetype2);
		});
		$(".phonetype3").change(function() {
			var phonetype3 = $(this).val();
			$('.phone3TypeValue').val(phonetype3);
		});
        
		// Get number of cards value and store in hidden input field for Google spreadsheet
		var cards = $("#inputCards").val();
		$('.cardsValue').val(cards);
		$(".cards").change(function() {
			var cards = $(this).val();
			$('.cardsValue').val(cards);
		});
        
		// Get shirt size value and store in hidden input field for Google spreadsheet
		$(".shirt").change(function() {
			var shirt = $('#inputShirt').val();
			$('.shirtValue').val(shirt);
		});
        
		// Disable Submit until all required fields are filled out
		$('.frmSubmit').prop('disabled', true);
        
		// Set the required fields value to blank so placeholder text doesn't interfere with required fields red toggle
		$(".businessCardForm .requiredField").each(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
			}
		});
        
		// Required fields change/keyup binding handler to enable Verify info checkbox once they are all filled out
		$(".businessCardForm .requiredField").bind("change keyup", function(event) {
			var requiredFieldsMet = false;
			var departmentValue = $('#inputDepartment').val();
			if (departmentValue == '') {
				$('#inputDepartment').addClass('required');
				requiredFieldsMet = false;
			} else {
				$('#inputDepartment').removeClass('required');
				requiredFieldsMet = true;
			}
			var fullnameValue = $('#inputCardFullname').val();
			if (fullnameValue == '') {
				$('#inputCardFullname').addClass('required');
				requiredFieldsMet = false;
			} else {
				$('#inputCardFullname').removeClass('required').removeClass('placeholder');
				if (requiredFieldsMet == false) {} else {
					requiredFieldsMet = true;
				}
			}
			var titleValue = $('#inputCardTitle').val();
			if (titleValue == '') {
				$('#inputCardTitle').addClass('required');
				requiredFieldsMet = false;
			} else {
				$('#inputCardTitle').removeClass('required').removeClass('placeholder');
				if (requiredFieldsMet == false) {} else {
					requiredFieldsMet = true;
				}
			}
			var addressLocationValue = $('#inputCardAddress1').val();
			if (addressLocationValue == '') {
				$('#inputCardAddress1').addClass('required');
				requiredFieldsMet = false;
			} else {
				if (requiredFieldsMet == false) {} else {
					$('#inputCardAddress1').removeClass('required');
					requiredFieldsMet = true;
				}
			}
			var emailValue = $('#inputCardEmail').val();
			if (emailValue == '') {
				$('#inputCardEmail').addClass('required');
				requiredFieldsMet = false;
			} else {
				$('#inputCardEmail').removeClass('required').removeClass('placeholder');
				if (requiredFieldsMet == false) {} else {
					requiredFieldsMet = true;
				}
			}
			var officeValue = $('#inputCardPhone1').val();
			if (officeValue == '') {
				$('#inputCardPhone1').addClass('required');
				requiredFieldsMet = false;
			} else {
				$('#inputCardPhone1').removeClass('required').removeClass('placeholder');
				if (requiredFieldsMet == false) {} else {
					requiredFieldsMet = true;
				}
			}
			var shirtsizeValue = $('#inputShirt').val();
			if (shirtsizeValue == '') {
				$('#inputShirt').addClass('required');
				requiredFieldsMet = false;
			} else {
				if (requiredFieldsMet == false) {} else {
					$('#inputShirt').removeClass('required');
					requiredFieldsMet = true;
				}
			}
            
			// Show or Hide "Verify Info" checkbox based on required fields being filled out
			if (requiredFieldsMet == true) {
				$('.verifyInfo').show();
			} else {
				$('.verifyInfo').hide();
			}
            
			//			if($(this).val() != '') {
			//				$(this).removeClass('required');
			//				$(".businessCardForm .requiredField").each(function() {
			//					if($(this).val() == '') {
			//						return false;
			//					}
			//					else {
			//						requiredFieldsMet = true;
			//					}
			//				});
			//				if (requiredFieldsMet == true) {
			//					$('input[type="submit"]').prop('disabled', false).removeClass('disabled');
			//				}
			//				else {
			//					$('.frmSubmit').prop('disabled', true).addClass('disabled');
			//				}
			//			}
			//			else {
			//				$(this).addClass('required');
			//				$('.frmSubmit').prop('disabled', true).addClass('disabled');
			//			}
		});
        
		// Finally enable Submit button based on user verifying information is correct and checking this box
		$("#inputVerifyInfo").change(function() {
			if (this.checked) {
				$('input[type="submit"]').prop('disabled', false).removeClass('disabled');
			} else {
				$('.frmSubmit').prop('disabled', true).addClass('disabled');
			}
		});
        
		// Validate and process form
		$(function() {
			$(".frmSubmit").click(function() {
				if ($("#inputCardCertification").val() == "Certification") {
					$("#inputCardCertification").val('');
				}
				if ($("#inputCardFullnameLanguage").val() == "First Last") {
					$("#inputCardFullnameLanguage").val('');
				}
				if ($("#inputCardCertificationLanguage").val() == "Certification") {
					$("#inputCardCertificationLanguage").val('');
				}
				if ($("#inputCardTitleLanguage").val() == "Job Title") {
					$("#inputCardTitleLanguage").val('');
				}
				if ($("#inputCardTitle2").val() == "2nd Title") {
					$("#inputCardTitle2").val('');
				}
				if ($("#inputCardAddress1").val() == "Choose...") {
					$("#inputCardAddress1").val('');
				}
				if ($("#inputCardAddress1Language").val() == "Choose...") {
					$("#inputCardAddress1Language").val('');
				}
				if ($("#inputCardTwitter").val() == "twitter handle") {
					$("#inputCardTwitter").val('');
				}
                
				var phonetype1 = $(".phonetype1").val();
				$('.phone1TypeValue').val(phonetype1);
				var phonetype2 = $(".phonetype2").val();
				$('.phone2TypeValue').val(phonetype2);
				var phonetype3 = $(".phonetype3").val();
				$('.phone3TypeValue').val(phonetype3);
                
				// Store double-sided checkbox value
				var doubleSidedCard = $("#inputCardDoubleSided").is(':checked');
				if (doubleSidedCard == true) {
					$(".doubleSidedValue").val("Double Sided Card");
				} else {
					$(".doubleSidedValue").val("")
				}
                
				// Display Please Wait message while form is being submitted and display Thank You message
				$("#businessCardForm").validate({
					meta: "validate",
					submitHandler: function(form) {
						var submitState = document.getElementById("ss-submit");
						submitState.value = "Please Wait";
						$('#loading_image').show();
						setTimeout(function() {
							$('#businessCardForm').toggle();
							form.submit();
							$('.hidden-message').show();
							$('.noCardToggle').hide();
						}, 1000);
					}
				});
			});
		});
        
        
		/*** NO BUSINESS CARD SECTION ***/
		// Get department value and store in hidden input field for Google spreadsheet
		$(".nodepartment").change(function() {
			var department = $(this).val();
			$('.noDepartmentValue').val(department);
		});
        
		// Get shirt size value and store in hidden input field for Google spreadsheet
		$(".noshirt").change(function() {
			var shirt = $('#inputNoShirt').val();
			$('.noShirtValue').val(shirt);
		});
        
		// Validate and process form
		$(function() {
			$(".frmNoSubmit").click(function() {
				if ($("#inputNoDepartment").val() == "Choose...") {
					$("#inputNoDepartment").val('');
				}
				if ($("#inputNoCardFullname").val() == "First Last") {
					$("#inputNoCardFullname").val('');
				}
				if ($("#inputNoShirt").val() == "Choose...") {
					$("#inputNoShirt").val('');
				}
                
				// Display Please Wait message while form is being submitted and display Thank You message
				$("#noBusinessCardForm").validate({
					meta: "validate",
					submitHandler: function(form) {
						var submitState = document.getElementById("no-ss-submit");
						submitState.value = "Please Wait";
						$('#loading_image1').show();
						setTimeout(function() {
							$('#noBusinessCardForm').toggle();
							form.submit();
							$('.hidden-message1').show();
							$('.noCardToggle').hide();
						}, 1000);
					}
				});
			});
		});
	});
})(jQuery);