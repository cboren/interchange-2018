<?php
/**
* Enqueues child theme stylesheet, loading first the parent theme stylesheet (adds time stamp in order to circumvent cacheing)
*/
function my_theme_enqueue_styles() {

    $parent_style = 'avada-stylesheet';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        time()
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

/**
* Enqueues child theme custom scripts
*/
function custom_child_enqueue_scripts() {
	
	wp_enqueue_script( 'theme-scripts', get_stylesheet_directory_uri() . '/js/scripts.js', array(), filemtime( get_stylesheet_directory() . '/js/scripts.js' ) );

}
add_action( 'wp_enqueue_scripts', 'custom_child_enqueue_scripts' );

function add_azalead_tracking_code(){ ?>

<!-- Azalead tracking code --> 
<script src="https://b2btagmgr.azalead.com/tag?az=aWRTdWJzY3JpYmVyPTE5NTgmaWRTaXRlPTIwNTEmb3JpZ2luPXd3dy5pdmFudGkuZnIvJmN1c3RvbT1ub25l" type="application/javascript"> </script> 
<!-- End Azalead tracking code -->

<?php } 

add_action('wp_footer', 'add_azalead_tracking_code');

function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' );
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

?>