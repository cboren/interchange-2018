<?php
/**
 * Header-2 template.
 *
 * @author     ThemeFusion
 * @copyright  (c) Copyright by ThemeFusion
 * @link       http://theme-fusion.com
 * @package    Avada
 * @subpackage Core
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
if(!(is_404())) {
	global $post;
	if($post->post_parent) {
		$event_date = get_field( 'event_date', $post->post_parent );
		$event_city = get_field( 'event_city', $post->post_parent );
		$event_street = get_field( 'event_street', $post->post_parent );
		$event_reg_url = get_field( 'event_reg_url', $post->post_parent );
		$event_button_text = get_field( 'event_button_text', $post->post_parent );
	} else {
		$event_date = get_field( 'event_date' );
		$event_city = get_field( 'event_city' );
		$event_street = get_field( 'event_street' );
		$event_reg_url = get_field( 'event_reg_url' );
		$event_button_text = get_field( 'event_button_text' );
	}
}
?>
<div class="fusion-header-sticky-height"></div>
<div class="fusion-sticky-header-wrapper"> <!-- start fusion sticky header wrapper -->
	<div class="fusion-header">
		<div class="fusion-row">
			<?php if ( 'flyout' === Avada()->settings->get( 'mobile_menu_design' ) ) : ?>
				<div class="fusion-header-has-flyout-menu-content">
				<?php endif; ?>
				<?php avada_logo(); ?>

				<?php if ( 'flyout' === Avada()->settings->get( 'mobile_menu_design' ) ) : ?>
					<?php get_template_part( 'templates/menu-mobile-flyout' ); ?>
				<?php else : ?>
					<?php get_template_part( 'templates/menu-mobile-modern' ); ?>
				<?php endif; ?>

				<?php if ( 'flyout' === Avada()->settings->get( 'mobile_menu_design' ) ) : ?>
				</div>
			<?php endif; ?>
			<?php if(!(is_404())) : ?>
				<?php if ($event_date && $event_city && $event_street && $event_reg_url) : ?>
					<div class="header-cta">
						<div class="header-cta-text"><?php echo $event_date; ?> &nbsp; | &nbsp; <?php echo $event_city; ?> &nbsp; | &nbsp; <?php echo $event_street; ?></div>
						<div class="cta">
							<a class="alizarin-crimson-hover-white-button square-button button" href="<?php echo $event_reg_url; ?>"><span><?php echo $event_button_text; ?></span></a>
						</div>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>