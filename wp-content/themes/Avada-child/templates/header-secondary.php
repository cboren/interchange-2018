<?php
$content_1 = avada_secondary_header_content( 'header_left_content' );
$content_2 = avada_secondary_header_content( 'header_right_content' );
?>

<div class="fusion-secondary-header">
	<div class="fusion-row">
		<nav class="lang col-xs-12" role="navigation" style="display: flex; justify-content: flex-end;">
			<div class="lang-menu-btn" for="lang-menu-state" style="text-algin: right;">
				<span class="lang-menu-btn-icon"></span>
				<ul id="lang-menu" class="sm sm-simple" style="padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 0px;">
					<li>
						<a href="https://interchange.ivanti.com/dallas/">Dallas Interchange</a>
					</li>
					<li>
						<a href="https://interchange.ivanti.com/madrid/">Madrid Interchange</a>
					</li>
					<hr class="menu-divider" />
					<li>
						<a href="https://interchange.ivanti.com/unplugged/paris/">Paris Unplugged</a>
					</li>
					<li>
						<a href="https://interchange.ivanti.com/unplugged/frankfurt/">Frankfurt Unplugged</a>
					</li>
					<li>
						<a href="https://interchange.ivanti.com/unplugged/munich/">Munich Unplugged</a>
					</li>
					<li>
						<a href="https://interchange.ivanti.com/unplugged/vienna/">Vienna Unplugged</a>
					</li>
					<li>
						<a href="https://interchange.ivanti.com/unplugged/berlin/">Berlin Unplugged</a>
					</li>
					<li>
						<a href="https://interchange.ivanti.com/unplugged/london/">London Unplugged</a>
					</li>
					<li>
						<a href="https://interchange.ivanti.com/unplugged/zurich/">Zurich Unplugged</a>
					</li>
				</ul>
			</div>
		</nav>
	</div>
</div>