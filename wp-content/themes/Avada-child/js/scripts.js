/* Javascripts */

jQuery(document).ready(function() {
	
	// Get an array of all element heights (make flip boxes the same height)
	var elementHeights = jQuery('.fusion-flip-boxes').children('.fusion-flip-box-wrapper').children('.fusion-flip-box').children('.flip-box-inner-wrapper').map(function() {
		return jQuery(this).height();
	}).get();
 
	// Math.max takes a variable number of arguments
	// <code>apply</code> is equivalent to passing each height as an argument
	var maxHeight = Math.max.apply(null, elementHeights)-54;
	// Set each height to the max height
  
	jQuery('.fusion-flip-boxes').children('.fusion-flip-box-wrapper').children('.fusion-flip-box').children('.flip-box-inner-wrapper').children('.flip-box-front').height(maxHeight);
 
	jQuery('.fusion-flip-boxes').children('.fusion-flip-box-wrapper').children('.fusion-flip-box').children('.flip-box-inner-wrapper').children('.flip-box-back').height(maxHeight);
	
	/* jQuery(".lang-menu-btn-icon").hover(function(){
		jQuery("ul#lang-menu").css("display", "block");
		}, function(){
		jQuery("ul#lang-menu").css("display", "none");
	}); */
	
	jQuery(".custom-select").each(function() {
	  var classes = jQuery(this).attr("class"),
		  id      = jQuery(this).attr("id"),
		  name    = jQuery(this).attr("name");
	  var template =  '<div class="' + classes + '">';
		  template += '<span class="custom-select-trigger">' + jQuery(this).attr("placeholder") + '</span>';
		  template += '<div class="custom-options">';
		  jQuery(this).find("option").each(function() {
			template += '<span class="custom-option ' + jQuery(this).attr("class") + '" data-value="' + jQuery(this).attr("value") + '">' + jQuery(this).html() + '</span>';
		  });
	  template += '</div></div>';
	  
	  jQuery(this).wrap('<div class="custom-select-wrapper"></div>');
	  jQuery(this).hide();
	  jQuery(this).after(template);
	});
	jQuery(".custom-option:first-of-type").hover(function() {
	  jQuery(this).parents(".custom-options").addClass("option-hover");
	}, function() {
	  jQuery(this).parents(".custom-options").removeClass("option-hover");
	});
	jQuery(".custom-select-trigger").on("click", function() {
	  jQuery('html').one('click',function() {
		jQuery(".custom-select").removeClass("opened");
	  });
	  jQuery(this).parents(".custom-select").toggleClass("opened");
	  event.stopPropagation();
	});
	jQuery(".custom-option").on("click", function() {
	  jQuery(this).parents(".custom-select-wrapper").find("select").val(jQuery(this).data("value"));
	  jQuery(this).parents(".custom-options").find(".custom-option").removeClass("selection");
	  jQuery(this).addClass("selection");
	  jQuery(this).parents(".custom-select").removeClass("opened");
	  jQuery(this).parents(".custom-select").find(".custom-select-trigger").text(jQuery(this).text());
	});
});